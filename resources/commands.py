import logging
from resources.codapi import *

#Para que funcionen los logs
logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(name)s - %(levelname)s - %(message)s,"
    )
logger = logging.getLogger()

#####   ARMAS   #####
def ak47(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por AK-47")
    update.message.reply_text("AK-47:\n\n-Para AK-47 de ColdWar /AK47CW\n-Para AK-47 de Modern Warfare /AK47MW")

def ak47cw(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por AK-47 CW")
    update.message.reply_text("AK-47 (ColdWar):\n\n-Silenciador GRU\n-Cañon Spetsnaz de 58.4 cm\n-Mira Axial Arms x3\n-Acople Empuñadura Spetsnaz\n-Municion de 45 balas\n\nPara AK-47 de Modern Warfare preguntar por /AK47MW")

def ak47mw(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por AK-47 MW")
    update.message.reply_text("AK-47 (Modern Warfare)):\n\n-Silenciador Monolitico\n-Cañon Rumano de 58.4 cm\n-Laser Tactico\n-Municion de 40 balas\n-Ventaja Prestidigitacion\n\nPara AK-47 de ColdWar preguntar por /AK47CW")

def ak74u(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por AK-74u")
    update.message.reply_text("AK74u:\n\n-Silenciador Gru\n-Cañon Fuerza Operativa de 40.6 cm\n-Culata de Armazon KGB\n-Municion Tambor Spetsnaz de 50 balas\n-Empuñadura Trasera de Serpiente\n\nAlternativa: reemplazar Culata de Armazon KGB por Mira LED Microflex")

def amp(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por AMP 63")
    update.message.reply_text("AMP 63:\n\nAkimbo:\n-Silenciador Agency\n-Cañon Fuerza Operativa de 18.3 cm\n-Laser Mira Laser SWAT de 5 mW\n-Culata a dos Manos\n-Municion Cargador Rapido Salvo de 23 balas")

def an94(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por AN94")
    update.message.reply_text("AN94:\n\n-Silenciador Monolitico\n-Cañon AN94 Estandar X de 438mm\n-Mira x3 VLK\n-Acople Frontal de Ranger\n-Municion Cargador de Caja Curvos de 60 balas")

def asval(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por AS-VAL")
    update.message.reply_text("AS-VAL:\n\n-Cañon VLK Osa de 200mm\n-Laser Tactico\n-Acople Frontal de Comando\n-Municion de 30 balas\n-Ventaja Prestidigitacion\n\nAlternativa: reemplazar prestidigitacion por totalmente cargado o Acople Frontal de Comando por Mercenario para tiro desde la cadera")

def aug(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por AUG")
    update.message.reply_text("AUG:\n\n-Para AUG de ColdWar /AUGCW\n-Para AUG de Modern Warfare /AUGMW")

def augcw(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por AUG CW")
    update.message.reply_text("AUG (ColdWar):\n\n-Silenciador Agency\n-Cañon Fuego Rapido de 45.7 cm\n-Mira Axial Arms x3\n-Acople Empuñadura Frontal de Agente de Campo\n-Municion Stanag de 54 balas\n\nPara AUG de Modern Warfare preguntar por /AUGMW")

def augmw(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por AUG MW")
    update.message.reply_text("AUG (Modern Warfare):\n\n-Silenciador Monolitico\n-Laser de 5 mW\n-Acople Frontal de Comando\n-Municion Cargadores de 32 balas\n-Empuñadura Trasera Adhesivo Punteado\n\nPara AUG de ColdWar preguntar por /AUGCW")

def ax50(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por AX-50")
    update.message.reply_text("AX-50:\n\n-Silenciador Monolitico\n-Cañon de Fabrica de 81.3 cm\n-Laser Tactico\n-Culata Singuard Arms Tactico\n-Empuñadura Trasera Adhesivo Punteado\n\nAlternativa: reemplazar Empuñadura Trasera por Municion de 9 balas")

def ballesta(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Ballesta")
    update.message.reply_text("Ballesta MW:\n\n-Cable de 28 Hebras\n-Arco XRK Thunder de 91 kg\n-Laser Tactico\n-Mira Reflex IG Mina\n-Municion Proyectiles FTAC Fury de 50.8 cm\n\nPara ballesta de ColdWar preguntar por /R1")

def bizon(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Bizon")
    update.message.reply_text("Bizon:\n\n-Silenciador Monolitico\n-Cañon Acero de 22.1 cm\n-Laser de 5 mW\n-Culata Sin Culata\n-Empuñadura Trasera Adhesivo Punteado\n\nPara Bizon de ColdWar preguntar por /Bullfrog")

def bruen(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Bruen MK9")
    update.message.reply_text("Bruen MK9:\n\n-Silenciador Monolitico\n-Cañon XRK Summit de 68.1 cm\n-Mira x3 VLK\n-Acople Empuñadura Frontal de Comando\n-Municion de 60 balas")

def bullfrog(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Bullfrog")
    update.message.reply_text("Bullfrog:\n\n-Silenciador Gru\n-Cañon Fuerza Operativa de 18.7 cm\n-Culata PKM Spetsnaz\n-Acople Empuñadura Spetsnaz\n-Empuñadura Trasera Vendaje de Serpiente\n\n(Viene con 50 balas)\n\nPara Bullfrog de Modern Warfare preguntar por /Bizon")

def c58(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por C58")
    update.message.reply_text("C58:\n\n-Silenciador Agency\n-Cañon Fuerza Operativa de 47 cm\n-Mira Axial x3\n-Acople Empuñadura de Agente de Campo\n-Municion Tambor de 45 balas")

def carv2(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Carv.2")
    update.message.reply_text("Carv.2:\n\n-Silenciador Agency\n-Cañon 52.8 cm de Equipo de Asalto\n-Mira Visiontech x2\n-Acople Empuñadura Frontal de Agente de Campo\n-Municion Tambor de 45 balas")

def cr56(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por CR-56")
    update.message.reply_text("CR-56 Amax:\n\n-Silenciador Monolitico\n-Cañon XRK Zodiac S440\n-Mira x3 VLK\n-Acople Frontal de Ranger\n-Municion de 45 balas")

def cx9(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por CX-9")
    update.message.reply_text("CX-9:\n\n-Cañon CX-38S\n-Laser Tactico\n-Culata CX-FA\n-Acople Frontal de Comando\n-Municion Tambor de 50 balas")

def diamatti(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Diamatti")
    update.message.reply_text("Diamatti:\n\nAkimbo:\n-Silenciador Agency\n-Cañon Fuerza Operativa de 18.3 cm\n-Laser Mira Laser SWAT de 5 mW\n-Culata a dos Manos\n-Municion Cargador Rapido Salvo de 30 balas\n\nAlternativa: Reemplazar Culata a dos Manos por Empuñadura Trasera Vendaje de Serpiente")

def dmr(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por DMR")
    update.message.reply_text("DMR:\n\n-Silenciador Agency\n-Cañon Fuerza Operativa de 52.8 cm\n-Mira Axial Arms x3\n-Acople Empuñadura Frontal de Agente de Campo\n-Municion Stanag de 40 balas")

def dragunov(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Dragunov")
    update.message.reply_text("Dragunov:\n\n-Silenciador Monolitico\n-Cañon Ampliado de 660 mm\n-Laser Tactico\n-Culata FTAC Cazador-Explorador\n-Municion Cargador de 20 balas")

def e725(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por 725")
    update.message.reply_text("725:\n\n-Silenciador Monolitico\n-Cañon Tempus de Competencia\n-Laser Tactico\n-Culata Recortada\n-Municion Cartuchos de Balas")

def ebr14(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por EBR-14")
    update.message.reply_text("EBR-14:\n\n-Silenciador Monolitico\n-Cañon Forge TAC de Precision de 55.9 cm\n-Mira x3 VLK\n-Acople Frontal de Comando\n-Municion de 20 balas")

def em2(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por EM2")
    update.message.reply_text("EM2:\n\n-Silenciador Agency\n-Cañon Fuerza Operativa de 65.5 cm\n-Mira Axial Arms x3\n-Acople Empuñadura de Operador de Campo\n-Municion Stanag de 50 balas\n\nAlternativa Subfusil:\n\n-Silenciador\n-Mira LED Microflex\n-Culata Raider\n-Acople Empuñadura de Operador de Campo\n-Municion de 40 balas")

def fal(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por FAL")
    update.message.reply_text("FN FAL:\n\n-Silenciador Monolitico\n-Cañon XRK de Tirador\n-Mira Reflex IG Mini\n-Acople Frontal de Comando\n-Municion de 30 balas\n\nAlternativa: reemplazar mira reflex IG por x3 VLK")

def famas(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por FAMAS")
    update.message.reply_text("FAMAS:\n\n-Para FAMAS de ColdWar /FFAR\n-Para FAMAS de Modern Warfare /FR556")

def fara(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por FARA 83")
    update.message.reply_text("FARA 83:\n\n-Silenciador GRU\n-Cañon Spetsnaz RPK de 47.5 cm\n-Mira Axial Arms x3\n-Acople Empuñadura Spetsnaz\n-Municion Spetsnaz de 50 balas")

def fennec(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Fennec")
    update.message.reply_text("Fennec:\n\n-Cañon ZLR Deadfall de 45.7 cm\n-Laser Tactico\n-Acople Empuñadura Frontal de Comando\n-Municion Tambor de 40 balas\n-Empuñadura Trasera Adhesivo Engomado")

def ffar(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por FFAR1")
    update.message.reply_text("FFAR1:\n\n-Silenciador Agency\n-Cañon Fuerza Operativa de 40.6 cm\n-Acople Empuñadura de Operador de Campo\n-Municion Stanag de 50 balas\n-Empuñadura Trasera Vendaje de Serpiente")

def finn(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por FINN LMG")
    update.message.reply_text("FINN LMG:\n\n-Silenciador Monolitico\n-Cañon XRK Longshot Adverso\n-Laser Tactico\n-Mira x3 VLK\n-Acople Frontal de Comando\n\nAlternativa: reemplazar mira por Municion Cintas de 75 balas 5.56 CT para movilidad")

def fr556(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por FR 5.56")
    update.message.reply_text("FR 5.56:\n\n-Silenciador Monolitico\n-Cañon FR de Francotirador de 62 cm\n-Mira x3 VLK\n-Acople Frontal de Comando\n-Municion de 50 balas")

def gallo(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Gallo")
    update.message.reply_text("Gallo SA12:\n\n-Estrangulador Agency\n-Cañon Reforzado Pesado de 54.3 cm\n-Culata Maraton\n-Municion Tambor Stanag de 12 balas\n-Empuñadura Trasera de Serpiente")

def grau(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Grau")
    update.message.reply_text("Grau:\n\n-Silenciador Monolitico\n-Cañon Tempus Arcangel de 67.1 cm\n-Mira x3 VLK\n-Acople Frontal de Comando\n-Municion de 60 balas\n\nAlternativa: reemplazar Mira por Laser Tactico")

def grav(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Grav")
    update.message.reply_text("Grav:\n\n-Silenciador Agency\n-Cañon Fuerza Operativa de 54.10 cm\n-Mira Axial Arms x3\n-Acople Empuñadura de Operador de Campo\n-Municion de 50 balas")

def groza(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Groza")
    update.message.reply_text("Groza:\n\n-Silenciador Gru\n-Cañon Grado Militar CMV de 42 cm\n-Acople Empuñadura Rapida Spetsnaz\n-Municion Tambor Spetsnaz de 60 balas\n-Culata Almohadilla de la KGB\n\nAlternativa: Cambiar Culata por Mira Axial Arms x3")

def hauer(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Hauer 77")
    update.message.reply_text("Hauer 77:\n\n-Boca de Cañon Estrangulador Agency\n-Cañon Fuerza Operativa de 40.6 cm\n-Laser Identificador de Objetivos FOE\n-Culata sin Culata\n-Municion Tubo de 7 balas")

def hdr(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por HDR")
    update.message.reply_text("HDR:\n\n-Silenciador Monolitico\n-Cañon HDR Pro de 68.3 cm\n-Laser Tactico\n-Mira Optica con Zoom Variable\n-Culata FTAC Campeon\n\nAlternativa: reemplazar Laser Tactico por Municion de 7 u 9 balas")

def holger(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Holger-26")
    update.message.reply_text("Holger-26:\n\n-Silenciador Monolitico\n-Laser Tactico\n-Mira x3 VLK\n-Acople Frontal de Comando\n-Empuñadura Trasera Adhesivo Punteado")

def ironhide(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Ironhide de Calibre 410")
    update.message.reply_text("Ironhide de Calibre 410:\n\n-Boca de Cañon Estrangulador Agency\n-Cañon Fuerza Operativa de 61.4 cm\n-Culata sin Culata\n-Municion Tubo Stanag de 8 proyectiles\n-Empuñadura Trasera Vendaje de Serpiente")

def iso(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por ISO")
    update.message.reply_text("ISO:\n\n-Cañon FSS Nightshade\n-Laser Tactico\n-Culata Plegable ISO\n-Acople Frontal de Comando\n-Municion Tambor de 50 balas")

def jak12(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por JAK12")
    update.message.reply_text("JAK12:\n\n-Boca de Cañon Forge TAC Merodeador\n-Cañon ZLR J-2800 Influx\n-Laser de 5 MW\n-Acople Frontal de Mercenario\n-Municion Cargador de Tambor de 20 balas")

def kar98(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Kar98")
    update.message.reply_text("Kar98:\n\n-Silenciador Monolitico\n-Cañon Singuard Personalizado de 63.8 cm\n-Mira Optica con Zoom Variable\n-Empuñadura Trasera Adhesivo Punteado\n-Ventaja Prestidigitacion\n\nAlternativa: reemplazar Ventaja Prestidigitacion por Laser Tactico y Mira Zoom Variable por Mira de Francotirador")

def kilo(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Kilo")
    update.message.reply_text("Kilo:\n\n-Silenciador Monolitico\n-Cañon Singuard Arms de Merodeador de 50.3 cm\n-Mira x3 VLK\n-Acople Frontal de Comando\n-Municion de 60 balas")

def krig(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Krig 6")
    update.message.reply_text("Krig 6:\n\n-Silenciador Agency\n-Cañon CMV Militar de 38.1 cm\n-Mira Axial Arms x3\n-Acople Empuñadura de Agente de Campo\n-Municion Stanag de 60 balas")

def ksp(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por KSP 45")
    update.message.reply_text("KSP 45:\n\n-Silenciador Agency\n-Cañon Fuerza Operativa de 40.6 cm\n-Mira LED Microflex\n-Acople Empuñadura Frontal de Agente de Campo\n-Municion Stanag de 48 balas")

def lapa(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por LAPA")
    update.message.reply_text("LAPA:\n\n-Silenciador Agency\n-Cañon Estriado de 20 cm\n-Mira LED Microflex\n-Municion Stanag de 50 balas\n-Empuñadura Trasera Vendaje de Serpiente")

def lc10(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por LC-10")
    update.message.reply_text("LC-10:\n\n-Silenciador Agency\n-Cañon Reforzado Pesado de 30.2 cm\n-Acople Empuñadura de Agente de Campo\n-Municion Stanag de 55 balas\n-Empuñadura Trasera de la Jungla SASR")

def m1(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por M1 Garand")
    update.message.reply_text("M1 Garand:\n\n-Todavia no disponible.")

def m13(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por M13")
    update.message.reply_text("M13:\n\n-Silenciador Monolitico\n-Cañon Tempus de Tirador\n-Mira x3 VLK\n-Acople Frontal de Comando\n-Municion de 60 balas")

def m16(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por M16")
    update.message.reply_text("M16:\n\n-Silenciador Agency\n-Cañon Fuerza Operativa de 52 cm\n-Mira Axial Arms x3\n-Acople Empuñadura Frontal de Agente de Comando\n-Municion Stanag de 60 balas")

def m19(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por M19")
    update.message.reply_text("M19:\n\nAkimbo:\n-Cañon XRK V Ampliado\n-Laser de 5 mW\n-Sistema de Gatillo Ligero\n-Municion de 32 balas\n-Ventaja Duales\n\nAlternativa: Reemplazar Ventaja Duales por Totalmente Cargado y Laser de 5 mW por Silenciador Monolitico")

def m4(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por M4")
    update.message.reply_text("M4:\n\n-Para M4 de ColdWar /XM4\n-Para M4 de Modern Warfare /M4A1")

def m4a1(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por M4A1")
    update.message.reply_text("M4A1 (Modern Warfare):\n\n-Silenciador Monolitico\n-Cañon M16 Granadero de serie\n-Mira x3 VLK\n-Acople Frontal de Comando\n-Municion de 60 balas\n\nAlternativa: reemplazar mira x3 VLK por Laser Tactico\n\nPara M4 de ColdWar preguntar por /XM4")

def m60(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por M60")
    update.message.reply_text("M60:\n\n-Silenciador\n-Cañon Calibre 57.9 de Competencia\n-Mira Axial Arms x3\n-Acople Empuñadura Frontal de Operador de Campo\n-Municion Cargador Acelerado de 120 balas")

def m82(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por M82")
    update.message.reply_text("M82:\n\n-Silenciador Cubierto\n-Cañon Combate de Reconocimiento de 57.4 cm\n-Laser Mira Laser de SWAT 5mW\n-Acople Empuñadura Frontal\n-Municion Cargador Aceler de 7 balas")

def m91(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por M91")
    update.message.reply_text("M91:\n\n-Silenciador Monolitico\n-Cañon Fuerzas Espaciales M91\n-Laser Tactico\n-Mira x3 VLK\n-Acople Frontal de Comando")

def mac(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por MAC-10")
    update.message.reply_text("MAC-10:\n\n-Silenciador Agency\n-Cañon Fuerza Operativa de 15 cm\n-Acople Empuñadura de Operador de Campo\n-Municion Tambor Stanag de 53 balas\n-Empuñadura Trasera de Serpiente\n\nAlternativa: reemplazar Empuñadura Trasera por Culata de Alambre y la Municion Stanag por cargador rapido (para disparo de la cadera)")

def magnum(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Magnum")
    update.message.reply_text("Magnum:\n\nAkimbo\n-Silenciador Agency\n-Cañon 11.9 cm de Cañon Corto Estrecho\n-Laser de Avistamiento Ember\n-Culata Duales\n-Municion Cargador Rapido de 12 balas Stanag")

def marshal(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Marshal")
    update.message.reply_text("Marshal:\n\nAkimbo:\n-Cañon Recortado de 16.51 cm\n-Culata a dos Manos\n-Municion Cargador Aliento de Dragon")

def mg34(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por MG34")
    update.message.reply_text("MG34:\n\n-Silenciador Ligero\n-Laser Tactico\n-Mira x3 VLK\n-Acople Frontal de Comando\n-Municion Cinta de 100 balas")

def mg82(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por MG 82")
    update.message.reply_text("MG 82:\n\n-Silenciador Agency\n-Cañon Recortado de 37.34 cm\n-Mira Axial Arms x3\n-Acople Empuñadura de Operador de Campo\n-Municion Cargador Acelerado de 125 balas\n\nAlternativa: reemplazar Cañon Recortado por Cañon de Fuerza Operativa para mas alcance pero menos movilidad")

def milano(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Milano")
    update.message.reply_text("Milano:\n\n-Silenciador Agency\n-Cañon Fuerza Operativa de 40.6 cm\n-Acople Empuñadura de Operador de Campo\n-Municion Tambor Stanag de 50 balas\n-Empuñadura Trasera de Serpiente")

def mk2(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por MK2 Carabina")
    update.message.reply_text("MK2 Carabina:\n\n-Silenciador Monolitico\n-Cañon FSS de 50.8 cm Estandar\n-Laser Tactico\n-Mira x3 VLK\n-Empuñadura Trasera Adhesivo Engomado")

def modelo(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Modelo 680")
    update.message.reply_text("Modelo 680:\n\n-Boca de Cañon Forge TAC Merodeador\n-Cañon XRK Deportivo de 76.2 cm\n-Laser Tactico\n-Culata sin Culata\n-Municion de Aliento de Dragon")

def mp5(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por MP5")
    update.message.reply_text("MP5:\n\n-Para MP5 de ColdWar /MP5CW\n-Para MP5 de Modern Warfare /MP5MW")

def mp5cw(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por MP5 CW")
    update.message.reply_text("MP5 (ColdWar):\n\n-Silenciador Agency\n-Culata Raider\n-Acople Empuñadura de Operador de Campo\n-Municion Tambor Stanag de 50 balas\n-Empuñadura Trasera Vendaje de Serpiente\n\nAlternativa: reemplazar culata por Cañon Reforzado Pesado de 24.1 cm\n\nPara MP5 de Modern Warfare preguntar por /MP5MW")

def mp5mw(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por MP5 MW")
    update.message.reply_text("MP5 (Modern Warfare):\n\n-Cañon Integrado Monolitico\n-Laser de 5 mW\n-Acople Frontal de Mercenario\n-Municion de 45 balas\n-Culata Plegable FTAC\n\nPara MP5 de ColdWar preguntar por /MP5CW")

def mp7(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por MP7")
    update.message.reply_text("MP7:\n\n-Silenciador Monolitico\n-Cañon FSS de Reconocimiento\n-Laser Tactico\n-Acople Frontal de Comando\n-Municion de 60 balas")

def oden(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Oden")
    update.message.reply_text("Oden:\n\n-Silenciador Monolitico\n-Cañon Oden Estandar de 810mm\n-Mira x3 VLK\n-Acople Frontal de Comando\n-Municion de 30 balas")

def ots9(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por OTs 9")
    update.message.reply_text("OTs 9:\n\n-Silenciador Agency\n-Cañon Fuerza Operativa de 20.57 cm\n-Culata de Armazon KGB\n-Municion Spetsnaz de 40 balas\n-Empuñadura Trasera Vendaje de Serpiente")

def origin(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Origin")
    update.message.reply_text("Origin 12:\n\n-Silenciador Monolitico\n-Cañon Forge TAC Precision\n-Laser de 5 MW\n-Culata sin Culata\n-Municion de 12 balas")

def p1911(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por 1911")
    update.message.reply_text("1911:\n\n-Para 1911 de ColdWar /1911CW\n-Para 1911 de Modern Warfare /1911MW")

def p1911cw(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por 1911 CW")
    update.message.reply_text("1911 (ColdWar):\n\n-Silenciador Agency\n-Cañon Fuerza Operativa de 16.6 cm\n-Laser Foco de Equipo de Tigre\n-Municion de 12 balas\n-Empuñadura Trasera de Serpiente\n\nPara 1911 de Modern Warfare preguntar por /1911MW")

def p1911mw(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por 1911 MW")
    update.message.reply_text("1911 (Modern Warfare):\n\nAkimbo:\n-Cañon 1911 Acechador\n-Laser de 5 mW\n-Sistema de Gatillo Ligero\n-Municion de 15 balas\n-Ventaja Duales\n\nPara 1911 de ColdWar preguntar por /1911CW")

def p357(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por .357")
    update.message.reply_text(".357:\n\nAkimbo:\n-Cañon Silverfield Ordnance cal. 357\n-Laser de 5 mW\n-Sistema de Gatillo Ligero\n-Municion Disparo de Perdigones\n-Ventaja Duales")

def p50gs(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por .50 GS")
    update.message.reply_text(".50 GS:\n\nAkimbo:\n-Cañon Forge TAC ampliado\n-Laser de 5 mW\n-Sistema de Gatillo Ligero\n-Municion de 13 balas\n-Ventaja Duales")

def p90(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por P90")
    update.message.reply_text("P90:\n\n-Silenciador Monolitico\n-Laser Tactico\n-Culata Correa Moderna\n-Acople Adhesivo Granulado\n-Empuñadura Trasera Adhesivo Punteado")

def pelington(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Pelington")
    update.message.reply_text("Pelington:\n\n-Silenciador Cubierto\n-Cañon Combate de Reconocimiento de 69 cm\n-Laser Mira Laser de SWAT 5mW\n-Municion Cargador Acelerado de 7 balas\n-Empuñadura Trasera Vendaje de Serpiente")

def pkm(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por PKM")
    update.message.reply_text("PKM:\n\n-Silenciador Monolitico\n-Cañon Compacto de 46.2 cm\n-Culata sin Culata\n-Acople Empuñadura Rapida\n-Municion Cinta de 200 balas")

def ppsh(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por PPSH-41")
    update.message.reply_text("PPSH-41:\n\n-Silenciador de Sonido\n-Cañon Fuerza Operativa de 39.9 cm\n-Acople Empuñadura Spetsnaz\n-Municion Tambor Spetsnaz de 55 balas\n-Empuñadura Trasera de Serpiente")

def qbz(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por QBZ-83")
    update.message.reply_text("QBZ-83:\n\n-Silenciador Agency\n-Cañon Fuerza Operativa 40.6 cm\n-Mira Axial Arms x3\n-Acople Empuñadura de Agente de Campo\n-Municion Stanag de 60 balas")

def r1shadowhunter(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por R1 Shadowhunter")
    update.message.reply_text("R1 Shadowhunter:\n\n-Mira LED Microflex\n\nPara Ballesta de Modern Warfare preguntar por /Ballesta")

def r9(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por R9")
    update.message.reply_text("R9-0:\n\n-Silenciador Monolitico\n-Cañon Forge TAC Centinela\n-Laser de 5 MW\n-Acople Empuñadura Frontal de Mercenario\n-Municion de Aliento de Dragon\n\nAlternativa: reemplazar Silenciador Monolitico por Boca de cañon Estrangulador")

def raal(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por RAAL")
    update.message.reply_text("RAAL:\n\n-Boca de Cañon RAAL de Nucleo Monolitico\n-Cañon RAAL Linea de Ruptura de 81.3 cm\n-Mira x3 VLK\n-Acople FSS oblicuo\n-Municion Cintas de 50 balas")

def ram(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por RAM-7")
    update.message.reply_text("RAM-7:\n\n-Silenciador Monolitico\n-Cañon FSS Ranger\n-Mira x3 VLK\n-Acople Frontal de Comando\n-Municion de 50 balas\n\nAlternativa Corto:\n-Silenciador Monolitico\n-Mira Reflex IG Mini\n-Laser Tactico\n-Acople Frontal de Comando\n-Municion de 50 balas")

def renetti(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Renetti")
    update.message.reply_text("Renetti:\n\nAkimbo:\n-Cañon MK1 Ampliado\n-Laser de 5 mW\n-Sistema de Gatillo Ligero\n-Municion de 27 balas\n-Ventaja Duales")

def rpd(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por RPD")
    update.message.reply_text("RPD:\n\n-Silenciador GRU\n-Cañon Fuerza Operativa de 51.5 cm\n-Mira Axial Arms x3\n-Culata de Armazon KGB\n-Acople Empuñadura Frontal\n\nAlternativa: reemplazar culata por Cargador Acelerado de 120 balas")

def rytec(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Rytec")
    update.message.reply_text("Rytec AMR:\n\n-Silenciador de Rytec AMR\n-Cañon FTAC Seven Straight\n-Laser Tactico\n-Municion Cargador de 25 x 59 mm de termita x5\n-Ventaja Claridad Mental")

def sa87(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por SA87")
    update.message.reply_text("SA87:\n\n-Silenciador Monolitico\n-Cañon SA87 de 64.5 cm Estandar\n-Mira x3 VLK\n-Acople Frontal de Comando\n-Municion Cargador de 60 balas")

def scar(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Scar-H")
    update.message.reply_text("FN Scar 17:\n\n-Silenciador Monolitico\n-Cañon Forge TAC de 50.8 cm\n-Mira x3 VLK\n-Acople Frontal de Comando\n-Municion de 30 balas")

def sks(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por SKS")
    update.message.reply_text("SKS:\n\n-Silenciador Monolitico\n-Cañon FSS M59/66 de 55.8 cm\n-Mira x3 VLK\n-Acople Frontal de Comando\n-Municion de 30 balas")

def spr(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por SP-R")
    update.message.reply_text("SP-R 208 - SP:\n\n-Silenciador Monolitico\n-Cañon SP-R de 66 cm\n-Laser Tactico\n-Mira Optica con Zoom Variable\n-Municion Cargadores de Calibre 300 Norma x5\n\nAlternativa: Mira Zoom Variable por Mira de Francotirador")

def stg(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por StG 44")
    update.message.reply_text("StG 44:\n\n-Todavia no disponible.")

def stoner(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Stoner 63")
    update.message.reply_text("Stoner 63:\n\n-Silenciador Agency\n-Cañon Lancero de Caballeria de 55.4 cm\n-Mira Axial Arms x3\n-Culata Almohadilla Raider\n-Acople Empuñadura Frontal de Agente de Campo\n\nAlternativa: reemplazar culata por Cargador Acelerado de 120 balas")

def streetsweeper(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Streetsweeper")
    update.message.reply_text("Streetsweeper:\n\n-Estrangulador Agency\n-Cañon Forjado a Mano de 33.8 cm\n-Laser Mira Laser de SWAT 5mW\n-Culata Marathon\n-Municion Stanag de 18 balas")

def striker(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Striker 45")
    update.message.reply_text("Striker 45:\n\n-Silenciador Monolitico\n-Cañon Acero Inoxidable de 400 mm\n-Culata FSS Guardian\n-Acople Frontal de Comando\n-Municion de 45 balas")

def swiss(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Swiss K31")
    update.message.reply_text("Swiss K31:\n\n-Silenciador GRU\n-Cañon 63.25 cm de Reconocimiento de Combate\n-Laser Mira Laser de SWAT 5mW\n-Municion Cargador Acelerado de 7 balas\n-Empuñadura Trasera Vendaje de Serpiente")

def sykov(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Sykov")
    update.message.reply_text("Sykov:\n\n-Silenciador Monolitico\n-Cañon Sorokin Automatica de 140mm\n-Laser Tactico\n-Municion Tambores de 80 balas\n-Ventaja Totalmente Cargado\n\nAlternativa: Reemplazar Ventaja Totalmente Cargado por Empuñadura Trasera VLK Prizrak")

def tec9(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por TEC-9")
    update.message.reply_text("TEC-9:\n\n-Boca de Cañon Repetidor Automatico\n-Cañon Fuerza Operativa de 12.5 cm\n-Acople Empuñadura de Agente de Campo\n-Municion Stanag de 48 balas\n-Empuñadura Trasera Vendaje de Serpiente")

def tundra(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por LW3 - Tundra")
    update.message.reply_text("LW3 - Tundra:\n\n-Silenciador Cubierto\n-Cañon Combate de Reconocimiento de 73.9 cm\n-Laser Mira Laser de SWAT 5mW\n-Municion Cargador Acelerado de 7 balas\n-Empuñadura Trasera de Serpiente")

def type63(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Type 63")
    update.message.reply_text("Type 63:\n\n-Silenciador\n-Cañon Calibre de 54.6 cm de Competencia\n-Mira Axial Arms x3\n-Acople Empuñadura Spetsnaz\n-Municion 30 balas")

def uzi(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Uzi")
    update.message.reply_text("Uzi:\n\n-Silenciador Monolitico\n-Cañon FSS de Carabina Pro\n-Culata Sin Culata\n-Acople Frontal de Mercenario\n-Municion de 50 balas")

def vlk(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Rogue VLK")
    update.message.reply_text("Rogue VLK:\n\n-Boca de Cañon Forge TAC Merodeador\n-Cañon VLK Zar\n-Laser Tactico\n-Culata sin Culata\n-Municion Aliento de Dragon de 8 Cartuchos")

def x16(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por X16")
    update.message.reply_text("X16:\n\nAkimbo:\n-Cañon Singuard Arms Ventaja\n-Laser de 5 mW\n-Sistema de Gatillo Ligero\n-Municion de 26 balas\n-Ventaja Duales")

def xm4(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por XM4")
    update.message.reply_text("XM4 (ColdWar):\n\n-Silenciador Agency\n-Cañon Fuerza Operativa de 34.3 cm\n-Mira Axial x3\n-Acople Empuñadura de Operador de Campo\n-Municion Stanag de 60 balas\n\nAlternativa Subfusil:\n\n-Silenciador\n-Cañon Fuerza Operativa de 34.3 cm\n-Mira LED Microflex\n-Acople Empuñadura de Operador de Campo\n-Municion de 45 balas\n\n(Se podria reemplazar Mira LED por Culata Almohadilla Raider)\n\nPara M4 de Modern Warfare preguntar por /M4A1")

def zrg(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por ZRG 20 mm")
    update.message.reply_text("ZRG 20 mm:\n\n-Silenciador Cubierto\n-Cañon Combate de Reconocimiento de 111.5 cm\n-Laser Mira Laser de SWAT 5mW\n-Municion Stanag de 7 balas\n-Empuñadura Trasera de la Jungla SASR")


#####   Streamers   #####
def aydan(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Aydan")
    update.message.reply_text("Armas de Aydan:\n\nhttps://docs.google.com/spreadsheets/u/0/d/1uAU48i0XY0n_bwLaNLE5yJEBN_qn9ufb9b7oYX6ZNFE/htmlview")

def mutex(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Mutex")
    update.message.reply_text("Armas de Mutex:\n\nhttps://docs.google.com/spreadsheets/u/0/d/1noCHj28dM6GArYcaGyN5DcJmVtwireoBRlBP7E4UTUo/htmlview")

def lobbyAmir(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Lobby Amir")
    update.message.reply_text(getLobbyTotalInfo("battle", "deusamir%231557"))

def lobbyAydan(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Lobby Aydan")
    update.message.reply_text(getLobbyTotalInfo("battle", "aydan%2311691"))

def lobbyFlexz(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Lobby Flexz")
    update.message.reply_text(getLobbyTotalInfo("battle", "flexz%232541"))

def lobbyIron(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Lobby Flexz")
    update.message.reply_text(getLobbyTotalInfo("battle", "Iron%2311745"))

def lobbyMirrey(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Lobby Mirrey")
    update.message.reply_text(getLobbyTotalInfo("battle", "xmirrey%231293"))

def lobbySoki(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Lobby Soki")
    update.message.reply_text(getLobbyTotalInfo("battle", "soki%2321161"))

def lobbyTaison(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Lobby Taison")
    update.message.reply_text(getLobbyTotalInfo("battle", "TaisonTV%232260"))


#####   BONUS   #####
def alla(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por alla")
    update.message.reply_text("Ella")

def botrapido(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por botrapido")
    update.message.reply_text("Tranquilo a mi no me apures que no soy tu sirviente, pelotudo")

def botconchatumadre(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por botconchatumadre")
    update.message.reply_text("Que te pasa boludo?")

def cod(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por COD")
    update.message.bot.send_photo(update.message.chat.id, photo='https://drive.google.com/file/d/1EZYtZdUJqdGddTEX_H7SdrrgMpimgZ8j/view?usp=sharing', caption="Vamo a juga?")

def conectados(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Conectados")
    update.message.reply_text(hormigator_friends())

def guarson(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Guarson")
    update.message.reply_text("Mas vale que si pa")

def manco(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Manco")
    update.message.reply_text("No le digan asi, se llama mandalorian")

def mancolorian(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Mancolorian")
    update.message.reply_text("Alla va, rusheando solo")

def quaqua(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Quaqua")
    update.message.reply_text("El buen socio vitalicio de FonoGay, si llamas al 515-3232 seguro lo encontras")


######COD-API#####################

#####   Stats   #####
def bolsonaro(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Bolsonaro")
    update.message.reply_text(info_Player("psn", "agu_q1988", "El Peluca Milei"))

def hormigator(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Hormigator")
    update.message.reply_text(info_Player("psn", "Hormigator1", "Hormigator"))

def leko(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Leko")
    update.message.reply_text(info_Player("psn", "Aleko22lp", "Leko"))

def luquitas(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Luquitas")
    update.message.reply_text(info_Player("psn", "luquitasc70", "LuquitasC70"))

def mandalorian(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Mandalorian")
    update.message.reply_text(info_Player("battle", "Mandalorian%2311726", "ElFisgonMorboson"))

def pablo(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Pablo")
    update.message.reply_text(info_Player("psn", "carripablin10", "Carripablin10"))

def rapax(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Rapax7")
    update.message.reply_text(info_Player("battle", "rapax7%231438", "Rapax7"))

#####   Lobbys  #####
def lobbyBolsonaro(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Lobby Bolsonaro")
    update.message.reply_text(getLobbyTotalInfo("psn", "agu_q1988"))

def lobbyHormigator(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Lobby Hormigator")
    update.message.reply_text(getLobbyTotalInfo("psn", "Hormigator1"))

def lobbyLeko(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Lobby Leko")
    update.message.reply_text(getLobbyTotalInfo("psn", "Aleko22lp"))

def lobbyLuquitas(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Lobby Luquitas")
    update.message.reply_text(getLobbyTotalInfo("psn", "luquitasc70"))

def lobbyMandalorian(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Lobby Mandalorian")
    update.message.reply_text(getLobbyTotalInfo("battle", "Mandalorian%2311726"))

def lobbyPablo(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Lobby Pablo")
    update.message.reply_text(getLobbyTotalInfo("psn", "carripablin10"))

def lobbyRapax(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Lobby Rapax7")
    update.message.reply_text(getLobbyTotalInfo("battle", "rapax7%231438"))

def matchmaking(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Matchmaking")
    update.message.reply_text("Explicacion del Matchmaking SBMM y EOMM:\n\nhttps://www.reddit.com/r/CODWarzone/comments/m16nl2/debunking_the_myths_testing_sbmm_and_eomm_in/")

def tableColour(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Lobby Colour Table")
    update.message.reply_text(lobbyColourTable())


#####   COMANDOS    #####
def armas(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Comandos de Armas")
    update.message.reply_text(
        "/Comandos para armas:\n\n"
        + "\nFusiles de Asalto:"
        + "\n-/AK47MW"
        + "\n-/AK47CW"
        + "\n-/AN94"
        + "\n-/ASVAL"
        + "\n-/C58"
        + "\n-/CR56"
        + "\n-/EM2"
        + "\n-/FAL"
        + "\n-/FARA"
        + "\n-/FFAR"
        + "\n-/FR556"
        + "\n-/Grau"
        + "\n-/Grav"
        + "\n-/Groza"
        + "\n-/Kilo"
        + "\n-/Krig"
        + "\n-/M1"
        + "\n-/M13"
        + "\n-/M4A1"
        + "\n-/Oden"
        + "\n-/QBZ"
        + "\n-/RAM"
        + "\n-/SCAR"
        + "\n-/STG"
        + "\n-/XM4"
        + "\n\nSubfusiles:"
        + "\n-/AK74U"
        + "\n-/AUGMW"
        + "\n-/Bizon"
        + "\n-/Bullfrog"
        + "\n-/CX9"
        + "\n-/Fennec"
        + "\n-/ISO"
        + "\n-/KSP"
        + "\n-/LAPA"
        + "\n-/LC10"
        + "\n-/MAC"
        + "\n-/Milano"
        + "\n-/MP5CW"
        + "\n-/MP5MW"
        + "\n-/MP7"
        + "\n-/OTS9"
        + "\n-/P90"
        + "\n-/PPSH"
        + "\n-/Striker"
        + "\n-/TEC9"
        + "\n-/Uzi"
        + "\n\nEscopetas:"
        + "\n-/725"
        + "\n-/Gallo"
        + "\n-/Hauer"
        + "\n-/Ironhide"
        + "\n-/JAK12"
        + "\n-/Modelo"
        + "\n-/Origin"
        + "\n-/R9"
        + "\n-/Street"
        + "\n-/VLK"
        + "\n\nAmetralladoras Ligeras:"
        + "\n-/Bruen"
        + "\n-/Finn"
        + "\n-/Holger"
        + "\n-/M60"
        + "\n-/M91"
        + "\n-/MG34"
        + "\n-/MG82"
        + "\n-/PKM"
        + "\n-/RAAL"
        + "\n-/RPD"
        + "\n-/SA87"
        + "\n-/Stoner"
        + "\n\nFusiles Tacticos:"
        + "\n-/AUGCW"
        + "\n-/Ballesta"
        + "\n-/Carv2"
        + "\n-/DMR"
        + "\n-/EBR14"
        + "\n-/Kar98"
        + "\n-/M16"
        + "\n-/MK2"
        + "\n-/R1"
        + "\n-/SKS"
        + "\n-/SPR"
        + "\n-/Type63"
        + "\n\nFusiles de Precision:"
        + "\n-/AX50"
        + "\n-/Dragunov"
        + "\n-/HDR"
        + "\n-/M82"
        + "\n-/Pelington"
        + "\n-/Rytec"
        + "\n-/Swiss"
        + "\n-/Tundra"
        + "\n-/ZRG"
        + "\n\nPistolas:"
        + "\n-/1911CW"
        + "\n-/1911MW"
        + "\n-/357"
        + "\n-/50GS"
        + "\n-/AMP"
        + "\n-/Diamatti"
        + "\n-/M19"
        + "\n-/Magnum"
        + "\n-/Marshal"
        + "\n-/Renetti"
        + "\n-/Sykov"
        + "\n-/X16"
    )

def comandos(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Comandos")
    update.message.reply_text(
        "/Hola hasta ahora los /Comandos disponibles son los siguientes:\n\n"
        + "\n-----/Armas-----\n"
        + "\nFusiles de Asalto:"
        + "\n-/AK47MW"
        + "\n-/AK47CW"
        + "\n-/AN94"
        + "\n-/ASVAL"
        + "\n-/C58"
        + "\n-/CR56"
        + "\n-/EM2"
        + "\n-/FAL"
        + "\n-/FARA"
        + "\n-/FFAR"
        + "\n-/FR556"
        + "\n-/Grau"
        + "\n-/Grav"
        + "\n-/Groza"
        + "\n-/Kilo"
        + "\n-/Krig"
        + "\n-/M1"
        + "\n-/M13"
        + "\n-/M4A1"
        + "\n-/Oden"
        + "\n-/QBZ"
        + "\n-/RAM"
        + "\n-/SCAR"
        + "\n-/STG"
        + "\n-/XM4"
        + "\n\nSubfusiles:"
        + "\n-/AK74U"
        + "\n-/AUGMW"
        + "\n-/Bizon"
        + "\n-/Bullfrog"
        + "\n-/CX9"
        + "\n-/Fennec"
        + "\n-/ISO"
        + "\n-/KSP"
        + "\n-/LAPA"
        + "\n-/LC10"
        + "\n-/MAC"
        + "\n-/Milano"
        + "\n-/MP5CW"
        + "\n-/MP5MW"
        + "\n-/MP7"
        + "\n-/OTS9"
        + "\n-/P90"
        + "\n-/PPSH"
        + "\n-/Striker"
        + "\n-/TEC9"
        + "\n-/Uzi"
        + "\n\nEscopetas:"
        + "\n-/725"
        + "\n-/Gallo"
        + "\n-/Hauer"
        + "\n-/Ironhide"
        + "\n-/JAK12"
        + "\n-/Modelo"
        + "\n-/Origin"
        + "\n-/R9"
        + "\n-/Street"
        + "\n-/VLK"
        + "\n\nAmetralladoras Ligeras:"
        + "\n-/Bruen"
        + "\n-/Finn"
        + "\n-/Holger"
        + "\n-/M60"
        + "\n-/M91"
        + "\n-/MG34"
        + "\n-/MG82"
        + "\n-/PKM"
        + "\n-/RAAL"
        + "\n-/RPD"
        + "\n-/SA87"
        + "\n-/Stoner"
        + "\n\nFusiles Tacticos:"
        + "\n-/AUGCW"
        + "\n-/Ballesta"
        + "\n-/Carv2"
        + "\n-/DMR"
        + "\n-/EBR14"
        + "\n-/Kar98"
        + "\n-/M16"
        + "\n-/MK2"
        + "\n-/R1"
        + "\n-/SKS"
        + "\n-/SPR"
        + "\n-/Type63"
        + "\n\nFusiles de Precision:"
        + "\n-/AX50"
        + "\n-/Dragunov"
        + "\n-/HDR"
        + "\n-/M82"
        + "\n-/Pelington"
        + "\n-/Rytec"
        + "\n-/Swiss"
        + "\n-/Tundra"
        + "\n-/ZRG"
        + "\n\nPistolas:"
        + "\n-/1911CW"
        + "\n-/1911MW"
        + "\n-/357"
        + "\n-/50GS"
        + "\n-/AMP"
        + "\n-/Diamatti"
        + "\n-/M19"
        + "\n-/Magnum"
        + "\n-/Marshal"
        + "\n-/Renetti"
        + "\n-/Sykov"
        + "\n-/X16"
        + "\n\n-----/Streamers-----"
        + "\n-/Aydan (Loadout de Aydan)"
        + "\n-/LobbyAmir"
        + "\n-/LobbyAydan"
        + "\n-/LobbyFlexz"
        + "\n-/LobbyIron"
        + "\n-/LobbyMirrey"
        + "\n-/LobbySoki"
        + "\n-/LobbyTaison"
        + "\n-/Mutex (Loadout de Mutex)"
        + "\n\n-----Bonus-----"
        + "\n-/Alla"
        + "\n-/Cod"
        + "\n-/Conectados"
        + "\n-/Guarson"
        + "\n-/Manco"
        + "\n-/Mancolorian"
        + "\n-/Quaqua"
        + "\n\n-----/Stats-----"
        + "\n\nStats:"
        + "\n-/Bolsonaro"
        + "\n-/Hormigator"
        + "\n-/Leko"
        + "\n-/Luquitas"
        + "\n-/Mandalorian"
        + "\n-/Pablo"
        + "\n-/Rapax"
        + "\n\nLobbys:"
        + "\n-/LobbyBolsonaro"
        + "\n-/LobbyHormigator"
        + "\n-/LobbyLeko"
        + "\n-/LobbyLuquitas"
        + "\n-/LobbyMandalorian"
        + "\n-/LobbyPablo"
        + "\n-/LobbyRapax"
        + "\n-/Matchmaking"
        + "\n-/Tabla"
        )

def hola(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Hola")
    name = update.effective_user['username']
    update.message.reply_text(f"Hola {name}, soy guarson, un bot creado para LMD2 con la intencion de dejar guardado todas las configuraciones de armas de Warzone, como asi tambien ofrecer estadisticas de las lobbies de los miembros del clan.")

def stats(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Comandos de Estadisticas")
    update.message.reply_text(
        "/Comandos para estadisticas:\n\n"
        + "\nStats:"
        + "\n-/Bolsonaro"
        + "\n-/Hormigator"
        + "\n-/Leko"
        + "\n-/Luquitas"
        + "\n-/Mandalorian"
        + "\n-/Pablo"
        + "\n-/Rapax"
        + "\n\nLobbys:"
        + "\n-/LobbyBolsonaro"
        + "\n-/LobbyHormigator"
        + "\n-/LobbyLeko"
        + "\n-/LobbyLuquitas"
        + "\n-/LobbyMandalorian"
        + "\n-/LobbyPablo"
        + "\n-/LobbyRapax"
        + "\n-/Matchmaking"
        + "\n-/Tabla"
        )

def streamers(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Comandos de Streamers")
    update.message.reply_text(
        "/Comandos para streamers:\n\n"
        + "\nStreamers:"
        + "\n-/Aydan (Loadout de Aydan)"
        + "\n-/LobbyAmir"
        + "\n-/LobbyAydan"
        + "\n-/LobbyFlexz"
        + "\n-/LobbyIron"
        + "\n-/LobbyMirrey"
        + "\n-/LobbySoki"
        + "\n-/LobbyTaison"
        + "\n-/Mutex (Loadout de Mutex)"
        )


#####   CERTIFICADO   ###
def certified(update, context):
    logger.info(f"El usuario {update.effective_user['username']}, consulto por Certificado")
    expire_date = int(os.getenv('sso_expire'))
    update.message.reply_text("Vence el: " + str(datetime.datetime.fromtimestamp(expire_date/1000)))